<?php

namespace Drupal\image_moderate\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting a image_moderate entity.
 *
 * @ingroup image_moderate
 */
class ImageModerateDeleteForm extends ContentEntityDeleteForm {

}
